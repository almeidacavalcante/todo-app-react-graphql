const db = require('../config/db')

const novoUsuario = {
    nome: 'Pedro',
    email: 'chama@pedro.com',
    senha: '12345678'
}

async function exercicio() {
    const { qtde } = await db('usuarios')
        .count('* as qtde').first()

    // await db('usuarios').insert(novoUsuario)
    const usuario = await db('usuarios')
        .where('email', 'like', '%chama@pedro.com%')
        .select()
        .first()

    console.log('Usuario: ', usuario);

    await db('usuarios')
        .where('email', usuario.email)
        .update({ nome: 'Pedro Alterado' })

    const usuarioAlterado = await db('usuarios')
        .where('email', 'like', '%chama@pedro.com%')
        .select().first()

    return await db('usuarios').where({ id: usuarioAlterado.id })
}

exercicio()
    .then(usuario => console.log(usuario))
    .finally(_ => db.destroy())