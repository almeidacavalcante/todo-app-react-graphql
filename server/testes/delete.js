const db = require('../config/db')

db('perfis').delete()
    .then(res => console.log(res))
    .finally(() => db.destroy());
