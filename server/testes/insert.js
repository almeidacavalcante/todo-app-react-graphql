const db = require('../config/db')

const table_perfis = 'perfis'
const novoPerfil = {
    nome: 'root',
    rotulo: 'Super Usuário'
}

db(table_perfis).insert(novoPerfil)
    .then(res => console.log(res))
    .catch(err => console.log(err))
    .finally(_ => db.destroy());