const { ApolloServer } = require('apollo-server')
const { importSchema } = require('graphql-import')
const resolvers = require('./src/resolvers')

const path = './src/schemas/index.graphql'
const server = new ApolloServer({
    typeDefs: importSchema(path),
    resolvers
})

server.listen().then(({ url }) => {
    console.log(`Executing at ${url}`);
})
