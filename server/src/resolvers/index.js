const Query = require('./Query')
const Mutation = require('./mutation')

module.exports = {
    Query,
    Mutation
}