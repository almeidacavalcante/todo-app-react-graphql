const db = require('../../config/db')

module.exports = {
    greetings() { return 'Welcome to the TODO APP GraphQL Api' },

    todos() {
        return db('todos')
            .where('active', true)
            .orderBy('id', 'desc')
            .then(response => response)
    },

    get_todo(id) {
        if (id) return db('todos').where({ id }).limit(1)
    }

}
