const db = require('../../../config/db')

module.exports = {
    async AddTodo(_, { data }) {

        try {
            const [id] = await db('todos').insert(data, ['id']);
            const todo = await db('todos').select().where(id).first();
            return todo;
        } catch (e) {
            throw new Error(e.sqlMessage)
        }
    },

    async UpdateTodo(_, { id, data: { description, priority, active } }) {

        try {
            await db('todos')
                .update({
                    description: description,
                    priority: priority,
                    active: active
                }).where({ id })

            const retorno = await db('todos').select().where({ id }).first();
            return retorno;
        } catch (e) {
            throw new Error(e.sqlMessage)
        }
    }
}
