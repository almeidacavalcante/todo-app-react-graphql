
exports.up = function (knex) {
    return Promise.all([
        knex.schema.createTable('tags', table => {
            table.increments('id').primary()
            table.string('name').notNull()
            table.string('font_color_hex').notNull().defaultTo('#FFFFFF')
            table.string('background_color_hex').notNull().defaultTo('#008DD5')
            table.timestamp('created_at').defaultTo(knex.fn.now())
            table.boolean('active').notNull().defaultTo(true)
        }).then(() => {
            return knex('tags').insert(
                [
                    { name: 'work' },
                    { name: 'personal' },
                    { name: 'study' },
                ]
            )
        }),
        knex.schema.createTable('todos_tags', table => {
            table.increments('id').primary()
            table.integer('todo_id').references('todos.id')
            table.integer('tag_id').references('tags.id')
            table.timestamp('created_at').defaultTo(knex.fn.now())
        })
    ])
};

exports.down = function (knex) {
    return Promise.all([
        knex.schema.dropTable('todos_tags'),
        knex.schema.dropTable('tags')
    ])
};
