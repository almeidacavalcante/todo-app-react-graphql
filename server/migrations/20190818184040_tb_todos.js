
exports.up = function (knex) {
    return knex.schema.createTable('todos', table => {
        table.increments('id').primary()
        table.string('description').notNull()
        table.integer('priority').notNull().defaultTo(1)
        table.boolean('active').notNull().defaultTo(true)
        table.timestamp('created_at').defaultTo(knex.fn.now())
    }).then(() => {
        return knex('todos').insert(
            [
                { description: 'My First Task!', priority: 2 },
                { description: 'Keep it up!', priority: 3 },
                { description: 'Last task!' },
            ]
        )
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('todos')
};
