import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';
import { ApolloProvider } from '@apollo/react-hooks';
import Home from './containers/home/home.page';
import SignIn from './containers/signin/signin.page';
import Landing from './containers/landing/landing.page';
import Navbar from './shared/navbar/navbar.component';
import { makeStyles } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import blue from '@material-ui/core/colors/blue';

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});

const useStyles = makeStyles(theme => ({
  app: {
    backgroundColor: theme.palette.background.application
  }
}))

const App = ({ client }) => {

  const classes = useStyles();

  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <ToastContainer />
        <Navbar />
        <main role="main" className='container'>
          <div className="app" >
            <Switch>
              <Route exact path="/signin" component={SignIn} />
              <Route exact path="/home" component={Home} />
              <Route exact path="/" component={Landing} />
            </Switch>
          </div>
        </main>
      </ThemeProvider>
    </ApolloProvider>
  );
}



export default App;
