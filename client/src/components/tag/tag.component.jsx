import React from 'react';
import { Chip } from "@material-ui/core";
import { withStyles } from '@material-ui/styles';

const CustomChip = withStyles(theme => ({
    root: {
        borderRadius: 3,
        border: '1px solid #d5dce2',
        background: '#eaedf1',
        fontFamily: 'Poppins',
        fontSize: 12,
        color: 'gray'
    },
    deleteIcon: {
        color: '#b1b6bb'
    }
}))(Chip)
const Tag = (props) => {
    let injectedProps = {};
    injectedProps.size = 'small';

    return <CustomChip {...injectedProps} {...props} />;
}

export default Tag;