import React from 'react';
import TodoCard from '../todo/todo-card.component';
import { makeStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import Masonry from 'react-masonry-component';

const useStyles = makeStyles({
    todos: {
        display: 'grid',
        columnGap: '15px',
        rowGap: '15px',
        boxSizing: 'border-box',
        gridTemplateColumns: 'repeat(3, calc(33.3333% - 10px))',
    },
    skeleton: {
        padding: '10px 20px'
    },
    checkSkeleton: {
        margin: 'auto 0 auto auto'
    },
    flexSkeleton: {
        display: 'flex',
        width: '100%'
    },
    tagSkeleton: {
        margin: '0 5px 0 0'
    },
    stackgrid: {
        position: 'relative',
        zIndex: 0,
        '& span': {
            zIndex: 0,
        }
    },

})
const masonryOptions = {
    transitionDuration: 0
};
const TodoList = ({ todos, loading, onChange }) => {
    const classes = useStyles();
    console.log('todos', todos);

    return (
        loading ?
            <div className={classes.todos}>
                {
                    [1, 2, 3].map(id =>
                        <Paper key={id} className={classes.skeleton}>
                            <Skeleton variant='text' height={5} width='40%' />
                            <Skeleton variant="text" height={118} />
                            <div className={classes.flexSkeleton}>
                                <Skeleton width={70} className={classes.tagSkeleton} />
                                <Skeleton width={70} className={classes.tagSkeleton} />
                                <Skeleton className={classes.checkSkeleton} variant="circle" width={30} height={30} />
                            </div>
                        </Paper>
                    )
                }
            </div >
            :
            <Masonry className={'my-gallery-class'} options={masonryOptions}>
                {todos.map(todo =>
                    <TodoCard key={(todo.id) ? todo.id : Math.random()} todo={todo} onChange={() => onChange(todo)} />
                )}
            </Masonry>
    )
}

export default TodoList;