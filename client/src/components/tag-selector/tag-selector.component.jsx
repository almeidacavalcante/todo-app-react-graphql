import React, { useEffect, useRef, useState } from 'react';
import { Paper, Divider, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Tag from '../tag/tag.component';
import cx from 'clsx';
import { useSprings } from 'react-spring';

const useStyles = makeStyles({
    container: {
        padding: 10,
        position: 'absolute',
        display: 'inline-block',
        width: 179,
        zIndex: 10000,
        boxShadow: '0px 0px 50px 0px rgba(82, 63, 105, 0.15)',
        opacity: 0,
        transition: 'all 300ms ease',
        pointerEvents: 'none',
        '& Button': {
            fontSize: 12,
            transition: 'all 300ms ease',
            margin: '10px 0 0 0'
        }
    },
    appear: {
        pointerEvents: 'auto',
        opacity: 1,
    },
    title: {
        fontWeight: 700,
        fontSize: 14,
        textAlign: 'center',
        padding: 5,
    },
    tags: {
        textAlign: 'center',
        padding: '10px 0'
    },
    tag: {
        margin: 2
    }
})

const cumulativeOffset = (element) => {
    var top = 0, left = 0;
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);

    return {
        top: top,
        left: left
    };
};

const TagSelector = ({ show, reference, anchorId, onClose }) => {

    const handleClick = e => {
        if (node.current) {
            if (node.current.contains(e.target)) {
                // inside click
                return;
            }
        }
        // outside click ... do whatever on click outside here ...
        onClose(reference);
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClick);
        return () => { document.removeEventListener("mousedown", handleClick); };
    }, [])

    const anchorElementPosition = () => {
        const element = document.getElementById(anchorId)
        if (element) {
            const { top, left } = cumulativeOffset(element);
            return { x: left, y: top }
        }
    }

    const classes = useStyles(anchorElementPosition());
    const node = useRef();

    return (
        <Paper className={cx([classes.container, show ? classes.appear : ''])} ref={node}>
            <div className={classes.title}>Assign Tags</div>
            <Divider></Divider>
            <div className={classes.tags}>
                <Tag className={classes.tag} label='work' />
                <Tag className={classes.tag} label='study' />
                <Tag className={classes.tag} label='personal' />
                <Tag className={classes.tag} label='meditation' />
                <Tag className={classes.tag} label='misc' />
                <Tag className={classes.tag} label='dev' />
                <Tag className={classes.tag} label='music' />
                <Tag className={classes.tag} label='readings' />
            </div>
            <Divider></Divider>
            <Button>New Tag</Button>
            <Button>Manage Tags</Button>
        </Paper>
    )
}

export default TagSelector