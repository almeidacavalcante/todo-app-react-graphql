import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    bookmark: {
        color: 'rgb(81, 105, 126)',
        fontSize: '20px',
    },
}));

const Bookmark = ({ bookmark, onMark }) => {

    const classes = useStyles();
    const faClass = bookmark ? `fa fa-bookmark ${classes.bookmark}` : `fa fa-bookmark-o ${classes.bookmark}`


    return (
        <i className={faClass} onClick={() => onMark(!bookmark)} aria-hidden="true"></i>
    );
}

export default Bookmark;