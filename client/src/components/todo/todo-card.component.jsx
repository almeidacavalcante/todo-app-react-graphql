import React, { useState } from 'react';
import { makeStyles, darken } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import cx from 'clsx';
import Moment from 'react-moment';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Tag from '../tag/tag.component';
import TagSelector from '../tag-selector/tag-selector.component';
import { useSpring, animated } from 'react-spring';

const useStyles = makeStyles({
    card: {
        willChange: 'transform, opacity',
        width: '32%',
        minHeight: 120,
        padding: '10px 20px',
        margin: 5,
        borderRadius: 5,
        boxShadow: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
        boxSizing: 'border-box',
        position: 'relative',
        transition: '0.3s all ease-in-out',
        cursor: 'pointer',
        '&:hover': {
            background: '#f5f6fd'
        }
    },
    done: {
        opacity: '0.5',
        background: '#dfebf1'
    },
    eyed: {
        willChange: 'transform, opacity',
        opacity: 1,
        transition: 'all 1500ms ease'
    },

    notEyed: {
        willChange: 'transform, opacity',
        opacity: 0,
        transition: 'all 300ms ease'
    },

    noInteraction: {
        pointerEvents: 'none'
    },

    cardEditing: {
        height: 'auto',
        transition: '0.5s all ease-in-out',
    },
    priorityContainer: {
        height: 'fit-content',
        width: 'fit-content',
        position: 'relative',
        transition: '0.5s all ease-in-out',
        display: 'flex',
    },
    priorityFlag: {
        height: 5,
        background: '#16dc98',
        width: 25,
        borderRadius: 2.5,
        margin: '0 5px 0 0',
        position: 'absolute',
        transition: '0.2s all ease-in-out',
        '&:hover': {
            background: darken('#16dc98', 0.2)
        }
    },
    priorityFlagEditing: {
        transition: '0.5s all ease-in-out',
        height: 10,
        opacity: 0.2,
    },
    priorityFlagHighlight: {
        opacity: 1,
    },
    red: {
        background: '#F22525',
        width: 25,
        borderRadius: 2.5,
        position: 'absolute',
        '&:hover': {
            background: darken('#F22525', 0.2)
        }
    },
    orange: {
        background: '#FF9D26',
        width: 25,
        borderRadius: 2.5,
        position: 'absolute',
        '&:hover': {
            background: darken('#FF9D26', 0.2)
        }
    },
    translateX30: {
        transform: 'translateX(30px)'
    },
    translateX60: {
        transform: 'translateX(60px)'
    },
    date: {
        color: '#AAA',
        fontSize: 13,
        marginBlockStart: '0.3em',
        transition: '0.5s all ease'
    },
    description: {
        color: '#70798b',
        marginBlockEnd: '0em',
        transition: '0.5s all ease',
        lineHeight: 2,
        padding: 1,
        margin: '20px 0 0 0'
    },
    descriptionArea: {
        outline: 'none',
        lineHeight: 2,
        color: '#70798b',
        background: 'transparent',
        borderRadius: '5px',
        fontSize: 16,
        fontFamily: 'Poppins',
        width: '100%',
        overflow: 'hidden',
        boxSizing: 'border-box',
        padding: '0 0 13px 0',
        border: '1px solid transparent'
    },
    descriptionInput: {
        outline: 'none',
        lineHeight: 2,
        color: '#70798b',
        background: 'transparent',
        borderRadius: '5px',
        border: '1px dashed #dfebf1',
        fontSize: 16,
        marginBlockEnd: '0em',
        fontFamily: 'Poppins',
        width: '100%',
        resize: 'none',
        overflow: 'hidden',
        padding: '0 0 2px 0',
        position: 'relative',
    },

    doButton: {
        width: 29,
        height: 29,
        fontSize: 16,
        borderRadius: '50%',
        border: '2px solid #16dc98',
        color: '#16dc98',
        background: 'white',
        boxSizing: 'border-box',
        alignContent: 'center',
        textAlign: 'center',
        position: 'absolute',
        bottom: 10,
        right: 10,
        cursor: 'pointer',
        padding: '1px 0 0 0',
        outline: 'none',
        transition: '0.3s all ease-in-out',
        '&:hover, disabled': {
            background: '#16dc98',
            color: 'white',
            borderColor: '#16dc98',
        }
    },
    doButtonEditting: {
        borderColor: '#16badc',
        background: '#16badc',
        color: 'white',
        '&:hover, disabled': {
            background: '#08a1c0',
            color: 'white',
            borderColor: '#08a1c0',
        }
    },
    eyeButton: {
        top: 0,
        border: '2px solid transparent',
        right: '-10px',
        background: 'transparent',
        color: '#16badc',
        '&:hover, disabled': {
            background: 'transparent',
            color: '#08a1c0',
            borderColor: '#08a1c0',
        }
    },
    tag: {
        margin: '0 2px 0 0'
    },
    plusTag: {
        color: 'grey',
    },
});



const TodoCard = ({ todo, onChange }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [isTagManaging, setIsTagManaging] = useState(false);

    const [flipped, set] = useState(false)
    const { transform, opacity } = useSpring({
        opacity: flipped ? 0 : 0,
        transform: `perspective(600px) rotateY(${flipped ? 180 : 0}deg)`,
        config: { duration: 1 }
    })

    const handleTagManager = () => {
        setIsTagManaging(true)
    }

    const classes = useStyles();

    const priorityClassEditingGreen = cx([
        classes.priorityFlag,
        `${(isEditing) ? classes.priorityFlagEditing : ''}`,
        `${(isEditing && todo.priority === 1) ? classes.priorityFlagHighlight : ''}`,
    ])

    const priorityClassEditingOrange = cx([
        classes.priorityFlag,
        classes.orange,
        classes.translateX30,
        `${(isEditing) ? classes.priorityFlagEditing : ''}`,
        `${(isEditing && todo.priority === 2) ? classes.priorityFlagHighlight : ''}`,
    ])

    const priorityClassEditingRed = cx([
        classes.priorityFlag,
        classes.red,
        classes.translateX60,
        `${(isEditing) ? classes.priorityFlagEditing : ''}`,
        `${(isEditing && todo.priority === 3) ? classes.priorityFlagHighlight : ''}`,
    ])

    const cardClass = cx([
        classes.card,
        `${(!todo.active) ? classes.done : ''}`,
        `${(isEditing) ? classes.cardEditing : ''}`,
    ])

    const faClass = cx([
        `${(todo.active) ? 'fa fa-check' : 'fa fa-pencil-square-o'}`
    ])

    const calendarStrings = {
        lastDay: '[Yesterday at] LT',
        sameDay: '[Today at] LT',
        nextDay: '[Tomorrow at] LT',
        lastWeek: '[last] dddd [at] LT',
        nextWeek: 'dddd [at] LT',
        sameElse: 'L'
    };

    // TODO: Get the textarea value do some clean up and submit... 
    const handleEdition = () => {
        if (todo.active) {
            setIsEditing(!isEditing)
            setIsTagManaging(false)
        };
    }

    const handleClose = (reference) => {
        if (reference === todo.id) {
            setIsTagManaging(false)
        }
    }

    const onEnterKeyPress = (e) => {
        e.stopPropagation();
        if (e.keyCode === 13) {
            setIsEditing(false)
            const text = e.target.value.replace(/\r?\n|\r|↵/mg, "");
            todo.description = text;
            onChange();
        }
    }

    const handleAction = () => {
        todo.active = !todo.active;
        if (isEditing) setIsEditing(false);
        onChange();
    }

    const textArea = <TextareaAutosize className={classes.descriptionInput}
        type="text" onKeyUp={onEnterKeyPress} autoFocus disabled={!todo.active}
        onClick={e => e.stopPropagation()} defaultValue={todo.description} />;

    const description = <div className={classes.descriptionArea}>{todo.description}</div>;

    const mainButton = <button type='button' className={classes.doButton} onClick={handleAction} ><i className={faClass}></i></button>;
    const editingButton = <>
        <button type='button' className={cx([classes.doButton, classes.doButtonEditting])}
            onClick={handleEdition}><i className="fa fa-arrow-right"></i></button>
        <button type='button' className={cx([classes.doButton, classes.eyeButton])}
            onClick={() => set(state => !state)}><i className="fa fa-eye-slash"></i></button>
    </>;


    // onClick={() => set(state => !state)} 
    const changePriority = (priority) => {
        todo.priority = priority;
        onChange();
    }

    const tags = [
        'work',
        'study',
        'personal'
    ]

    const random_tags = () => {
        return [
            tags[Math.floor(Math.random() * tags.length)],
        ]
    }

    const handleNewTag = () => {
        console.log('new tag');
    }

    const open = isEditing && isTagManaging
    const idTagDots = `tag-dots-${todo.id}`;
    const tagElement = <Tag id={idTagDots} onClick={handleTagManager} label={<i className='fa fa-ellipsis-h'></i>} />

    return (
        <animated.div className={cardClass} style={{ opacity: opacity.interpolate(o => 1 - o), transform }}
            onClick={() => flipped ? set(state => !state) : ''}>
            <div className={flipped ? classes.noInteraction : ''}>
                <div className={cx([flipped ? classes.notEyed : classes.eyed])}>
                    <div className={classes.priorityContainer}>
                        <div className={classes.priorityContainer}>
                            {(isEditing || todo.priority === 1) ? <div className={priorityClassEditingGreen} onClick={() => changePriority(1)}></div> : ''}
                            {(isEditing || todo.priority === 2) ? <div className={priorityClassEditingOrange} onClick={() => changePriority(2)}></div> : ''}
                            {(isEditing || todo.priority === 3) ? <div className={priorityClassEditingRed} onClick={() => changePriority(3)}></div> : ''}
                        </div>
                    </div>

                    <div className={classes.description} onClick={handleEdition}>{(isEditing && todo.active) ? textArea : description}</div>
                    <p className={classes.date}><Moment calendar={calendarStrings}>{todo.created_at}</Moment></p>

                    {/* Rendering tags */}
                    {random_tags()
                        .map(tag => <Tag key={Math.random()} className={classes.tag} label='work' onDelete={() => console.log('delete tag...')} />)}

                    {isEditing ? tagElement : ''}
                    <TagSelector show={open} anchorId={idTagDots} reference={todo.id} onClose={(reference) => handleClose(reference)} />
                    {(isEditing && todo.active) ? editingButton : mainButton}

                </div>

            </div>

        </animated.div>
    );
}

export default TodoCard;