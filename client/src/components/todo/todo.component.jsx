import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import './todo.css'
import Bookmark from '../common/bookmark/bookmark';

const useStyles = makeStyles(theme => ({
    todoContainer: {
        margin: '4px',
        display: 'flex',
        height: '59px',
        minWidth: '200px',
        background: '#eff2f3',
        color: 'rgb(81, 105, 126)',
        borderRadius: '4px',
        overflow: 'hidden',
    },

    done: {
        background: '#eff9ff',
    },

    description: {
        flex: '1',
        alignSelf: 'center',
        margin: '0px 10px',
    },

    checkButton: {
        alignSelf: 'center',
        margin: '0px 10px',
    },

    priorityLow: {
        width: '6px',
        background: 'green',
    },

    priorityMid: {
        width: '6px',
        background: 'blue',
    },

    priorityHigh: {
        width: '6px',
        background: 'red',
    },

    addon: {
        background: '#1493b9',
        width: '20px',
        height: '20px',
        color: 'white',
        fontWeight: '500',
        fontSize: '12px',
        borderRadius: '2px',
        alignSelf: 'center',
        textAlign: 'center',
        margin: '0 10px 0 0',
    },

    bookmark: {
        alignSelf: 'center',
        margin: '3px 10px 0 10px',
    }
}));

const Todo = ({ todo, onChange }) => {

    const classes = useStyles();
    const [localTodo, setLocalTodo] = useState(todo);

    const handleBookmark = (bookmark) => {
        localTodo.mark = bookmark
        setLocalTodo(localTodo)
        onChange(localTodo)
    }

    const handleCompletion = () => {
        if (localTodo.state === 'undone')
            localTodo.state = 'done'

        onChange(localTodo);
    }

    const getPriorityClass = ({ priority }) => {
        if (priority === 'low') return classes.priorityLow
        if (priority === 'mid') return classes.priorityMid
        if (priority === 'high') return classes.priorityHigh
    }

    const getDoneClass = ({ state }) => {
        if (state === 'done') return `${classes.todoContainer} ${classes.done}`
        if (state === 'undone') return `${classes.todoContainer}`
    }

    return (
        <Grid container key={localTodo.id} className={getDoneClass(localTodo)}>
            <Grid item className={getPriorityClass(localTodo)}></Grid>
            <Grid item className={classes.checkButton}>
                <button disabled={localTodo.state === 'done'} className='btn btn-terciary'
                    onClick={handleCompletion}
                    type="checkbox" name="state" id={localTodo.id}>
                    <i className="fa fa-check"></i>
                </button>
            </Grid>
            <Grid item className={classes.description}>{localTodo.description}</Grid>
            <Grid item className={classes.bookmark}>
                <Bookmark bookmark={localTodo.mark} onMark={handleBookmark} />
            </Grid>
            <Grid item className={classes.addon}>{localTodo.addon}</Grid>
        </Grid>
    );
}

Todo.propTypes = {
    todo: PropTypes.exact({
        id: PropTypes.string.isRequired.isRequired,
        description: PropTypes.string.isRequired,
        priority: PropTypes.oneOf(['low', 'mid', 'high']).isRequired,
        state: PropTypes.oneOf(['done', 'undone']).isRequired,
        addon: PropTypes.number.isRequired,
        mark: PropTypes.boolean,
    }).isRequired,
}

Todo.defaultProps = {
    todo: {
        id: undefined,
        description: undefined,
        priority: 'low',
        state: 'undone',
        addon: undefined,
        mark: false,
    }
}

export default Todo;