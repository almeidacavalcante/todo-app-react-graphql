import http from './http.service'
import config from '../config/config.json'
import messageService from './message.service'

const login = async (credentials) => {
    try {
        const { data } = await http.post(config.api + 'login', credentials);
        const { access_token, refresh_token, message } = data;
        sessionStorage.setItem('access_token', access_token)
        sessionStorage.setItem('refresh_token', refresh_token)
        messageService.success(message);
        return true;
    } catch (err) {
        const { message } = err.response.data
        messageService.error(message)
    }
}

const logout = async () => {
    try {
        const { data } = await http.get(config.api + 'logout');
        messageService.success(data.message);
    } catch (err) {
        const { message } = err.response.data
        messageService.error(message)
    }
}

export default {
    login,
    logout
};