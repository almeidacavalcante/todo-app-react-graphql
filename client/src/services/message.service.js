import { toast } from 'react-toastify';

const success = (message, options) => {
    toast.success(message, options)
}

const error = (message, options) => {
    toast.error(message, options)
}

const info = (message, options) => {
    toast.info(message, options)
}

const warning = (message, options) => {
    toast.warn(message, options)
}

export default {
    success,
    error,
    info,
    warn: warning
}