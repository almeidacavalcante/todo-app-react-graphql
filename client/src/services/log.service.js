import http from './http.service';
import config from "../config/config.json";

const logApi = config.api + 'logger';

let reference = undefined;

const init = () => {
    reference = {
        release: config.release,
        environment: config.environment
    }
}

const log = (err) => {
    if (reference) {
        const logger = {
            ref: reference,
            err: err
        }
        http.post(logApi, logger)
    } else {
        throw new Error('You need to initialize the log service first.')
    }
}

export default log;