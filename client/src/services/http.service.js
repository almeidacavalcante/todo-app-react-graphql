import axios from 'axios';
import config from '../config/config.json'
import messageService from './message.service';

const client = axios.create({
    baseURL: config.api
})

client.defaults.headers['Content-Type'] = 'application/json;charset=utf-8';
client.interceptors.response.use(null, err => {
    const { status } = err.response;
    const expectedError = err.response && status >= 400 && status < 500;
    if (!expectedError) {
        console.log('Logging the unexpected error:', err);
        messageService.error('Um unexpected error ocurred.');
    }
    return Promise.reject(err);
})


export default {
    get: client.get,
    post: client.post,
    put: client.put,
    delete: client.delete,
}