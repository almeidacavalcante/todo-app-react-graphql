export const changeValue = (e) => {
    return {
        type: 'VALUE_CHANGED',
        payload: e.target.value
    }
}

export const createTodo = (e) => {
    return {
        type: 'TODO_CREATED',
        payload: {
            description: e.target.description,
            priority: e.target.priority,
            active: e.target.active,
        }
    }
}
