import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/StarBorder';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useQuery, useMutation } from '@apollo/react-hooks';
import TodoList from '../../components/todo-list/todo-list.component';
import { TextField, withStyles } from '@material-ui/core';
import { ADD_TODO_MUTATION, UPDATE_TODO_MUTATION } from '../../gql/mutations/mutations';
import { TODOS_QUERY } from '../../gql/queries/queries';
import messageService from '../../services/message.service';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { changeValue, createTodo } from './landing-actions'

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
        ul: {
            margin: 0,
            padding: 0,
        },
        li: {
            listStyle: 'none',
        },
    },
    textField: {
        display: 'flex',
    },
    button: {
        display: 'flex',
        margin: '16px 0 8px 0',
        height: 55,
        width: '100%',
        background: '#16dc98',
        color: 'white',
        '&:hover': {
            background: '#08b177',
        }
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        marginTop: '10px',
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
        backgroundColor: theme.palette.grey[200],
    },
    cardPricing: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing(2),
    },
    footer: {
        borderTop: `1px solid ${theme.palette.divider}`,
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        [theme.breakpoints.up('sm')]: {
            paddingTop: theme.spacing(6),
            paddingBottom: theme.spacing(6),
        },
    },
}));

const tiers = [
    {
        title: 'Free',
        price: '0',
        description: ['10 users included', '2 GB of storage', 'Help center access', 'Email support'],
        buttonText: 'Sign up for free',
        buttonVariant: 'outlined',
    },
    {
        title: 'Pro',
        subheader: 'Most popular',
        price: '15',
        description: [
            '20 users included',
            '10 GB of storage',
            'Help center access',
            'Priority email support',
        ],
        buttonText: 'Get started',
        buttonVariant: 'contained',
    },
    {
        title: 'Enterprise',
        price: '30',
        description: [
            '50 users included',
            '30 GB of storage',
            'Help center access',
            'Phone & email support',
        ],
        buttonText: 'Contact us',
        buttonVariant: 'outlined',
    },
];

const footers = [
    {
        title: 'Company',
        description: ['Team', 'History', 'Contact us', 'Locations'],
    },
    {
        title: 'Features',
        description: ['Cool stuff', 'Random feature', 'Team feature', 'Developer stuff', 'Another one'],
    },
    {
        title: 'Resources',
        description: ['Resource', 'Resource name', 'Another resource', 'Final resource'],
    },
    {
        title: 'Legal',
        description: ['Privacy policy', 'Terms of use'],
    },
];

const ATextField = withStyles({
    root: {
        '& label.Mui-focused': {
            color: '#2196f3',
            fontFamily: 'Poppins'
        },
        '& .MuiFilledInput-underline': {
            borderBottom: 'silver'
        },
        '& .MuiFilledInput-underline:after': {
            borderBottomColor: '#2196f3',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'red',
            },
            '&:hover fieldset': {
                borderColor: 'yellow',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#2196f3',
            },
        },
    },
})(TextField);

const Landing = (props) => {
    const { loading, error, data } = useQuery(TODOS_QUERY);
    const [todos, setTodos] = useState(() => [])

    useEffect(() => {
        if (!loading && props.todos.length === 0) setTodos(data['todos'])
    }, [loading, data, props.todos])

    const [AddTodo] = useMutation(ADD_TODO_MUTATION);
    const [UpdateTodo] = useMutation(UPDATE_TODO_MUTATION);

    const [todo] = useState({ description: '', priority: 1 })

    // Holds the references of the inputs...
    const form = {}

    // Depending on the name of the input, it holds its reference in form.
    const update = (e) => {
        form[e.target.name] = e.target;
    }

    const resetForm = () => {
        changeValue('');
        // if (form['description']) form['description'].value = '';
    }

    const updateTodo = (todo) => {
        UpdateTodo({
            variables: {
                id: todo.id,
                data: {
                    description: todo.description,
                    priority: todo.priority,
                    active: todo.active
                }
            }
        }).then(({ data: { UpdateTodo: todo } }) => {
            if (!todo.active) {
                messageService.success('Great! You just completed a task!')
                _removeTodoLocally(todo);
            }
        })
    }

    const _removeTodoLocally = (todo) => {
        const tmp = [...todos];
        const idx = tmp.findIndex(t => t.id === todo.id);
        setTimeout(() => {
            tmp.splice(idx, 1);
            setTodos([...tmp]);
        }, 300);
    }

    const handleChange = (todo) => {
        updateTodo(todo)
    }

    const createTodo = (todo) => {
        AddTodo({
            variables: { data: { description: todo.description, priority: todo.priority } },
        }).then(({ data }) => {
            const tmp = [...props.todos];
            tmp.shift();
            tmp.unshift(data.AddTodo);
            setTodos([...tmp]);
        });
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        // UI updates and cleanup
        if (form['description']) todo.description = form['description'].value;
        todos.unshift({ ...todo })
        setTodos([...todos])
        resetForm();

        try {
            createTodo(todo);
            messageService.success('A new task was created!');
        } catch (error) {
            messageService.error('Something went wrong!');
            throw error;
        }
    }

    const classes = useStyles();

    if (error) return `Error! ${error.message}`;

    return (
        <React.Fragment>

            <Container maxWidth="md" component="main" className={classes.heroContent}>
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={10}>
                            <ATextField
                                id="outlined-uncontrolled"
                                label="Task description"
                                className={classes.textField}
                                margin="normal"
                                variant="filled"
                                name='description'
                                autoComplete='off'
                                // onChange={update}
                                onChange={props.changeValue}
                                value={props.value}
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <Button type='submit' variant="contained" className={`${classes.button}`}>Add Task</Button>
                        </Grid>
                        <Grid item xs={12} id='todos-container'>
                            <TodoList todos={props.todos} onChange={handleChange} loading={loading}></TodoList>
                        </Grid>
                    </Grid>
                </form>

            </Container>

            <Container maxWidth="md" component="main">
                <Grid container spacing={5} alignItems="flex-end">
                    {tiers.map(tier => (
                        // Enterprise card is full width at sm breakpoint
                        <Grid item key={tier.title} xs={12} sm={tier.title === 'Enterprise' ? 12 : 6} md={4}>
                            <Card>
                                <CardHeader
                                    title={tier.title}
                                    subheader={tier.subheader}
                                    titleTypographyProps={{ align: 'center' }}
                                    subheaderTypographyProps={{ align: 'center' }}
                                    action={tier.title === 'Pro' ? <StarIcon /> : null}
                                    className={classes.cardHeader}
                                />
                                <CardContent>
                                    <div className={classes.cardPricing}>
                                        <Typography component="h2" variant="h3" color="textPrimary">
                                            ${tier.price}
                                        </Typography>
                                        <Typography variant="h6" color="textSecondary">/mo</Typography>
                                    </div>
                                    <ul>
                                        {tier.description.map(line => (
                                            <Typography component="li" variant="subtitle1" align="center" key={line}>
                                                {line}
                                            </Typography>
                                        ))}
                                    </ul>
                                </CardContent>
                                <CardActions>
                                    <Button fullWidth variant={tier.buttonVariant} color="primary">
                                        {tier.buttonText}
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>

            {/* Footer */}
            <Container maxWidth="md" component="footer" className={classes.footer}>
                <Grid container spacing={4} justify="space-evenly">
                    {footers.map(footer => (
                        <Grid item xs={6} sm={3} key={footer.title}>
                            <Typography variant="h6" color="textPrimary" gutterBottom>
                                {footer.title}
                            </Typography>
                            <ul>
                                {footer.description.map(item => (
                                    <li key={item}>
                                        <Link href="#" variant="subtitle1" color="textSecondary">
                                            {item}
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </Grid>
                    ))}
                </Grid>
            </Container>
            {/* End footer */}

        </React.Fragment >
    );
};

const mapStateToProps = (state) => {
    return {
        value: state.field.value,
        todos: state.todo.list
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ changeValue, createTodo }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
