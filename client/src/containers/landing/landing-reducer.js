import { combineReducers } from "redux";

const INITIAL_STATE = { value: '' }

const landingReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'VALUE_CHANGED':
            return { value: action.payload }

        default:
            return state
    }
}

const todoReducer = () => ({
    list: [
        {
            _id: 1,
            description: 'Pagar fatura do cartão',
            priority: 1,
            active: true,
        }, {
            _id: 2,
            description: 'Assistir aula sobre redux',
            priority: 2,
            active: false,
        }, {
            _id: 3,
            description: 'Comprar livros...',
            priority: 1,
            active: true,
        }
    ]
})

const rootReducers = combineReducers({
    field: landingReducer,
    todo: todoReducer
})

export default rootReducers