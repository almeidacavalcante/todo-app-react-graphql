import React, { useEffect } from 'react';
import TodoList from "../../components/todo-list/todo-list.component";
import { useState } from "react";
import { Grid } from '@material-ui/core';
import { ExchangeRates } from '../../components/currency/exchange.component';


const Home = () => {

    useEffect(() => {

    }, []);

    const [todos, setTodos] = useState(
        [
            {
                "id": "1",
                "description": "Buy a notebook",
                "priority": "low",
                "state": "undone",
                "addon": 3,
                "mark": true
            },
            {
                "id": "2",
                "description": "Study React",
                "priority": "high",
                "state": "undone",
                "addon": 2,
                "mark": false
            },
            {
                "id": "3",
                "description": "Review Fleshcards",
                "priority": "high",
                "state": "undone",
                "addon": 5,
                "mark": true
            }
        ]
    );

    const handleChange = (todo) => {
        const index = todos.indexOf(todo);
        const localTodos = [...todos];
        localTodos[index] = todo;
        setTodos(localTodos);
    }

    return (
        <React.Fragment>
            <Grid container>
                <Grid item xl={12}>
                    <TodoList todos={todos} onChange={handleChange}></TodoList>
                </Grid>
            </Grid>

            {/* <Grid container spacing={2}>
                <Grid item>
                    <ExchangeRates showOnly={6}></ExchangeRates>
                </Grid>
            </Grid> */}
        </React.Fragment>
    );
}

export default Home;