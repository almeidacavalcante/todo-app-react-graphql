import gql from 'graphql-tag';

const ADD_TODO_MUTATION = gql`
    mutation AddTodo($data: TodoInput!) {
        AddTodo(data: $data) {
            id,
            description,
            priority,
            active,
            created_at
        }
    }
`

const UPDATE_TODO_MUTATION = gql`
    mutation UpdateTodo($id: Int!, $data: TodoInput!) {
        UpdateTodo(id: $id, data: $data) {
            id,
            description,
            priority,
            active,
            created_at
        }
    }
`

export { ADD_TODO_MUTATION, UPDATE_TODO_MUTATION }