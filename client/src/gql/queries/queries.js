import gql from 'graphql-tag';

const TODOS_QUERY = gql`{
    todos {
        id
        description
        priority
        active
        created_at
    }
}`

const GET_TODO_QUERY = gql`{
    get_todo(id: Int) {
        id
        description
        priority
        active
        created_at
    }
}`


export { TODOS_QUERY, GET_TODO_QUERY }