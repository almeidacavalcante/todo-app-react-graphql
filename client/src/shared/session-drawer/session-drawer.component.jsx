import React, { useState } from 'react';
import { Drawer, withStyles, Icon, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import TabsComponent from '../tabs/tabs.component';
import clsx from 'clsx';
import { deepPurple, grey } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    list: {
        width: 400
    },
    fullList: {
        width: "auto"
    },
    icon: {
        color: theme.palette.common.defaultGray,
        fontSize: 16
    },
    closeIcon: {
        color: grey[400],
        fontSize: 16,
        "&:hover": {
            color: 'white'
        }
    },
    closeButton: {
        minWidth: 30,
        minHeight: 30,
        margin: 10,
        position: 'absolute',
        right: 0,
        zIndex: 999,
        backgroundColor: grey[100],
        "&:hover ": {
            backgroundColor: deepPurple[200]
        },
        "&:hover *": {
            color: 'white !important',
        }
    }
}));

const SDrawer = withStyles(theme => ({
    root: {
        color: 'white',
    },
}))(Drawer)

const SessionDrawer = (props) => {

    const [drawer, setDrawer] = useState(false, 'right');
    const classes = useStyles();

    const toggleDrawer = (side, open) => event => {
        if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift"))
            return;
        setDrawer({ open, side })
    };

    const sideList = side => (

        <div
            className={classes.list}
            onKeyDown={toggleDrawer(side, false)}
        >
            <Button className={classes.closeButton} onClick={toggleDrawer()}>
                <Icon className={clsx(classes.closeIcon, 'fa fa-times')} />
            </Button>
            <TabsComponent></TabsComponent>
        </div>
    );

    return (
        <div>
            <IconButton
                edge="end"
                aria-label="account of current user"
                aria-haspopup="true"
                onClick={toggleDrawer('right', true)}
                className={classes.icon}
            >
                <AccountCircle />
            </IconButton>
            <SDrawer anchor="right" open={drawer['open']} onClose={toggleDrawer('right', false)}>
                {sideList('right')}
            </SDrawer>
        </div >
    )
}

export default SessionDrawer;
