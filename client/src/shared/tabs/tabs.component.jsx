import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/styles';


const useStyles = makeStyles(theme => ({
    tabs: {
        width: 'auto'
    }
}));

const STab = withStyles({
    root: {
        padding: '0 20px',
        minWidth: 10
    }

})(Tab);

const STabs = withStyles(theme => ({
    root: {
        width: '400px',
        borderBottom: `2px solid ${theme.palette.common.lightGray}`,
        margin: '40px 0 0 0',
    },
}))(Tabs);

export default function TabsComponent() {
    const [value, setValue] = React.useState(2);
    const cls = useStyles();

    function handleChange(event, newValue) {
        setValue(newValue);
    }

    return (
        <STabs
            value={value}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleChange}
            className={cls.tabs}
        >
            <STab label="Notifications" fullWidth={true} />
            <STab label="App Logs" fullWidth={true} />
            <STab label="Settings" fullWidth={true} />
        </STabs>
    );
}