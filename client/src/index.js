import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import promise from 'redux-promise'

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'font-awesome/css/font-awesome.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import config from './config/config.json';
import rootReducers from './containers/landing/landing-reducer';

// TODO: Extract the theme to another module...
const theme = createMuiTheme();
theme.shadows[24] = '0 0 40px 0 rgba(82,63,105,.1)';
theme.palette.background.application = '#f9f9fc';
theme.palette.common.defaultGray = '#c9d0d8';
theme.palette.common.lightGray = '#f0f0f0';

const client = new ApolloClient({
    uri: config.apollo,
})

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const store = applyMiddleware(promise)(createStore)(rootReducers, devTools)

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <BrowserRouter>
            <Provider store={store}>
                <App client={client} />
            </Provider>
        </BrowserRouter>
    </MuiThemeProvider>
    ,
    document.getElementById('root'));
serviceWorker.unregister();
